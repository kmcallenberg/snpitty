# SNPitty

SNPitty is a visualization tool for displaying loss-of-heterozygosity and/or allelic imbalance (LOH/AI) by making use of .VCF files originating from primarily diagnostic panels, although it works on any input .VCF file.

SNPitty is written in R and uses Shiny and various other CRAN and BioConductor packages which are automatically installed (if not already present) upon starting the application. 

Shiny is an emerging technology provided by the RStudio project team which enables the creation of interactive and reactive web-applications directly based on R code.
This is a great addition to the R code-base as this allows the direct use of a great number of validated and established (BioConductor) packages for bioinformatics use.

# Table of Contents
1. [Docker Installation](#markdown-header-docker)
2. [Manual Installation](#markdown-header-manual-installation)
3. [Usage](#markdown-header-usage)


# Installation

Due to the requirement of the latest R and BioConductor versions, it is **highly advised** to use the pre-compiled Docker image.

## Docker

A pre-made [Docker image](https://hub.docker.com/r/ccbc/snpitty/) for SNPitty is available to ease the installation and quickly setup the entire application.

Simply download Docker for [Windows](https://www.docker.com/products/docker-toolbox) or [Linux](https://docs.docker.com/linux/) and pull the SNPitty image:
```
docker pull ccbc/snpitty
```

You can now run SNPitty using the following command:
```
# You need to specify an unused port. Port 3382 is also used to host a local exporting server.
docker run -p <yourport>:3383 -p 3382:3382 --name docker_snpitty ccbc/snpitty

# E.g.
# docker run -p 1234:3383 -p 3382:3382 --name docker_snpitty ccbc/snpitty
```

SNPitty will now be available on the computer which is running the docker container under the following URL:
```{HTML}
# In this example docker is running on the local machine, else use the IP of the server which is running SNPitty.
http://127.0.0.1:1234
```

### Manual Installation

You can also download and install SNPitty separate from the pre-installed docker environment an an R package.
SNPitty can be downloaded directly from the development branch on BitBucket using the following command in **R**:
```
# Require/install devtools package if not already installed.
if (!require("devtools")) install.packages("devtools", repos = "http://cran.r-project.org")
# Install SNPitty from BitBucket
devtools::install_bitbucket(repo = "ccbc/snpitty")

# Download all required packages
library(SNPitty)
SNPitty::loadSNPittyPackages()
```

SNPitty houses an internal function (```loadSNPittyPackages()```) to automatically download and install its dependencies.

To enable VCF file merging, **bcftools**, **tabix** and **bgzip** need to be installed in `/usr/bin/`.
Please follow the independent manuals for each tool to do so.

To start SNPitty from R, use the following command:
```
# For external access, an open TCP port must be given.
SNPitty::startSNPitty(host='0.0.0.0', port=<yourPort>)

# E.g.
# SNPitty::startSNPitty(host='0.0.0.0', port=1234)
```

#### Automatic reporting
To enable report functionality the following software needs to be installed:

* pdflatex
    *  Linux: sudo apt-get install texlive-latex-base (Or relevant package manager)
    *  Windows: Download [Miktex](http://miktex.org/download)

#### Local Highcharts exporting server
SNPitty uses the HighCharts JS library to create interactive plots. These plots can be exported as SVG/PNG/PDF images, a local exporting server should be running on port 3382. Please see [here for manual instructions](http://www.highcharts.com/docs/export-module/setting-up-the-server). The Docker already has this functionality installing and running in port 3382.

#### Windows

Windows support is limited as most of the underlying tools are made with Unix-systems in mind.
Testing shows that the main functions such as uploading VCF/GTF and visualization BAF are working. However, the following functions work only sparsely on Windows:

* VCF merging.
* Export function.

***

## Usage
Each connection to SNPitty is run on an independent session which allows multiple users to connect simultanously whilst separating data to each respective session.
Input files can be uploaded manually using the upload forms on their respectdive pages but can also be given as GET parameters in the URL in the following manner:

```{HTML}
For single VCF file with additional region file:
http://<HOST>:<PORT>/?vcfFile=<vcfFileLocation>&regionFile=<regionFileLocation>
```
Example:
```{HTML}
http://127.0.0.1:8080/?vcfFile=192.168.1.20/sampleX/panelXYZ.vcf&regionFile=192.168.1.20/annotation/panelXYZ.txt
```

These files must be accessible from the network or must be available on the machine running SNPitty.

### Regions
GTF files containing regional information on the variants can be added. This will allow users to view/filter on regions, similar to viewing/filtering on chromosomes.
E.g. a GTF file of the genes can be given and users can then visualize EGFR, p53 and PTEN only. Variants will be added to regions based on genomic overlap.

### Sessions
SNPitty uses temporary sessions, each new connection to SNPitty (browsing to SNPitty) will create a new session. This enables data-separation and allows for multiple users to make use of SNPitty on the same server. After closing the connection to SNPitty (closing the browser/tab) **all data will be lost** for that session. (Both on client and server)

### Settings
There are several settings available from the `settings` page in SNPitty. These are only remembered for the current session.
Another method to provide session-specific settings is to provide these in the URL, e.g:
* http://127.0.0.1:8080/?ratioHomoLineThreshold=0.05;0.9&zoomType=T&logReads=T&markVariants=G/A

The following settings can be altered via URL:

* ratioHomoLineThreshold, e.g.: http://127.0.0.1:8080/?ratioHomoLineThreshold=0.1;0.9
* ratioHeteroLineThreshold, e.g.: http://127.0.0.1:8080/?ratioHeteroLineThreshold=0.4;0.6
* filterOnID, e.g.: http://127.0.0.1:8080/?filterOnID=onlyRS
* flipData, e.g.: http://127.0.0.1:8080/?flipData=T
* zoomType, e.g.: http://127.0.0.1:8080/?zoomType=T
* logReads, e.g.: http://127.0.0.1:8080/?logReads=T
* showPattern, e.g.: http://127.0.0.1:8080/?showPattern=T
* showRegionsOnStart, e.g.: http://127.0.0.1:8080/?showRegionsOnStart=T
* markVariants, e.g.: http://127.0.0.1:8080/?markVariants=rs*
* infoDP, e.g.: http://127.0.0.1:8080/?infoDP=100
* infoSB, e.g.: http://127.0.0.1:8080/?infoSB=0.4;0.6

