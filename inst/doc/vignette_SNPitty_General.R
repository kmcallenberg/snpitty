## ----eval=FALSE----------------------------------------------------------
#  # Require/install devtools package if not already installed.
#  if (!require("devtools")) install.packages("devtools", repos = "http://cran.r-project.org")
#  # Install SNPitty from BitBucket
#  devtools::install_bitbucket(repo = "https://bitbucket.org/ccbc/snpitty")

## ----eval=FALSE----------------------------------------------------------
#  # For external access, an open TCP port must be given.
#  SNPitty::startSNPitty(host='0.0.0.0', port=<yourPort>)

