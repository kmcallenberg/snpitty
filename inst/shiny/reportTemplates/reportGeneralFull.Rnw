% !Rnw weave = Knitr
\documentclass{report}

\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{epsfig}
\usepackage{titlesec}
\usepackage{graphicx}
\usepackage[table,xcdraw]{xcolor}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{pdflscape}
\usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{adjustbox}
\usepackage{caption}
\usepackage{longtable}
\usepackage{tocloft}
\usepackage[a4paper, total={6in, 8in}]{geometry}
\usepackage{fullpage}

% Layout of hyperlinks.
\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  urlcolor=blue,
  citecolor=blue
}

\newcommand{\vsp}{\vspace{5 mm}}

\newcommand{\beginsupplement}{%
  \setcounter{table}{0}
  \renewcommand{\thetable}{S\arabic{table}}%
  \setcounter{figure}{0}
  \renewcommand{\thefigure}{S\arabic{figure}}%
}

<<loadPackages, include=FALSE, eval=TRUE, echo=F, cache=F>>=
  opts_chunk$set(fig.pos='H')

library(GenomicRanges, quietly = T)
library(Gviz, quietly = T)
library(knitr, quietly = T)
library(VariantAnnotation, quietly = T)
library(xtable, quietly = T)
library(ggplot2)

# Get the data from SNPitty
inputData <- dataReport

##### Filtering #####
# Min. 100 reads per variant.
geno(inputData)$ALTR[ geno(inputData)$DP < 100 ] <- -0.05

# Strand bias between 0.05 & 0.95
geno(inputData)$ALTR[!(geno(inputData)$SBV > 0.05 & geno(inputData)$SBV < 0.95) ] <- -0.05

# Only use annotated variants.
inputData <- inputData[!grepl(":.*_.*/", rownames(inputData))]

# Make long names shorter by splitting on _
rownames(inputData) <- unlist(lapply(strsplit(rownames(inputData), "_"), function(x) x[[1]]))

@
  
  <<external-code, cache=F, echo=F, message = F, warning=F>>=
  source('functions_reportGBM.R')
@
  
  \begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Front Page                              %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage
\thispagestyle{empty}
\begin{center}
\vspace*{1cm}
{\Huge \center \textcolor{cyan}{\bf{SNPitty}} \\ BAF Viewer \\ \small{Generated report} \\}

\vspace*{2cm}
{\Large Date:\\
[1mm] \textcolor{cyan}{\today }\\ }
\vfill

% Name of facility
{\it Cancer Computational Biology Center\\  Erasmus Medical Center\\  The Netherlands\\  [1mm] \today } \\

% ErasmusMC Logo
\vspace*{1cm}

\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               TOC Page                                %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage
\pagenumbering{gobble}

% Tables of content, figures, tables
\tableofcontents
\clearpage
\listoftables
\listoffigures
\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           General information                         %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Begin numbering at 1
\pagenumbering{arabic}
\setcounter{page}{1}

% Set title format
\definecolor{gray75}{gray}{0.75}
\newcommand{\hsp}{\hspace{20pt}}
\titleformat{\chapter}[hang]{\Huge\bfseries}{\thechapter\hsp\textcolor{gray75}{|}\hsp}{0pt}{\Huge\bfseries}

\chapter{General Information}
\begin{flushleft}
This is an automated report generated by SNPitty, it shows some of the possibilities which could be visualized in a report.\\
This report has been generated based on R/Latex and thus all BioConductor and R functionalities can be used to create a multitude of visualizations to suit specific needs.

\end{flushleft}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Sample information                          %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Samples}
\begin{flushleft}

<<overviewInput, results='asis', echo=F, message = F, warning=F, cache=F>>=
  createSampleOverview(inputData)
@
  
  A total of \Sexpr{nrow(inputData@colData)} sample(s) has been analyzed in this report, see \ref{tab:overviewSamples}. 
Additional information on the samples can be found in the supplementary information.\\

\vspace*{1cm}
These samples have been analyzed on loss-of-heterozygosity (LOH) by making use of informative heterozygote variants, see S1.

\end{flushleft}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Variant information                         %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Variant Information}
\section{Filtering information}
The variants have been filtered using the following criteria: >100 DP, strand bias must be between 0.05 - 0.95, hotspot variants only.\\

\section{Targeted coverage per chromosome}

<<covPerChr, fig.pos='H', fig.height=5, fig.ref='fig:varPerChr', fig.cap='The coverage of the distinct variants of this panel are shown per chromosome.', echo=F, message=F, cache=F>>=
  createBarplotCoverage(inputData)
@
  
  This panel has \Sexpr{nrow(inputData)} variants covering \Sexpr{length(seqlevelsInUse(inputData))} chromosomes.

\section{Location SNV per chromosome}
<<ideogramVariants, echo=F,message=F, warning=F, fig.pos='H', fig.height=8, fig.ref='fig:ideogramVariants', fig.cap='Ideogram depecting the location of each distinct variant on the genome.', echo=F, message=F, cache=F>>=
  createIdeogramVariants(inputData)
@
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Region information                        %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Region(s) of interest}
The following section contains pre-defined regions-of-interest which you can define yourself based on the supplied GTF or whole chromosomes.

\begin{landscape} 

  <<allChrInUse, echo=F,message=F, warning=F, out.width='10in', fig.width=16, fig.height=9, results='asis'>>=
    # Loop over each chromosome and check if regions are present.
    for(chr in seqlevelsInUse(inputData)){
      inputDataSub <- keepSeqlevels(inputData, chr)
      if(nrow(inputDataSub) != 0){
        if(is.null(info(inputDataSub)$REG)){
          info(inputDataSub)$REG <- as.character(seqnames(inputDataSub))
        }
    
        info(inputDataSub)$REG[is.na(info(inputDataSub)$REG)] <- as.character(seqnames(inputDataSub[is.na(info(inputDataSub)$REG)]))
        
        regions <- unique(info(inputDataSub)$REG)
        multiplot(createLOHPlotRegions(inputDataSub, regions,  plotTitle = paste0("BAF of variants in regions: ", chr), markVariants = markVariants), createReadBarplotRegions(inputDataSub, regions, plotTitle = paste0("BAF of variants in regions: ", chr), markVariants = markVariants))
        
        cat("\\newpage")
        }
    }
    

  
  @

\end{landscape}
\end{document}