% !Rnw weave = Knitr

\documentclass{report}

\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{epsfig}
\usepackage{titlesec}
\usepackage{graphicx}
\usepackage[table,xcdraw]{xcolor}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{pdflscape}
\usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{adjustbox}
\usepackage{caption}
\usepackage{longtable}
\usepackage{tocloft}

% Layout of hyperlinks.
\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  urlcolor=blue,
  citecolor=blue
}

\newcommand{\vsp}{\vspace{5 mm}}

\newcommand{\beginsupplement}{%
        \setcounter{table}{0}
        \renewcommand{\thetable}{S\arabic{table}}%
        \setcounter{figure}{0}
        \renewcommand{\thefigure}{S\arabic{figure}}%
     }

<<loadPackages, include=FALSE, eval=TRUE, echo=F, cache=F>>=
  opts_chunk$set(fig.pos='H')

  library(GenomicRanges, quietly = T)
  library(Gviz, quietly = T)
  library(knitr, quietly = T)
  library(VariantAnnotation, quietly = T)
  library(xtable, quietly = T)
  library(ggplot2)

  # Get the data from SNPitty
  inputData <- dataReport
  
  ##### Filtering #####
  # Min. 100 reads per variant.
   geno(inputData)$ALTR[ geno(inputData)$DP < 100 ] <- -0.05
  
  # Strand bias between 0.05 & 0.95
  geno(inputData)$ALTR[!(geno(inputData)$SBV > 0.05 & geno(inputData)$SBV < 0.95) ] <- -0.05
  
  # Only use annotated variants.
  inputData <- inputData[!grepl(":.*_.*/", rownames(inputData))]
  
  # Make long names shorter by splitting on _
  rownames(inputData) <- unlist(lapply(strsplit(rownames(inputData), "_"), function(x) x[[1]]))

@

<<external-code, cache=FALSE, echo=F, message = F, warning=F>>=
  source('functions_reportGBM.R')
@

\begin{document}

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                               Front Page                              %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  \newpage
  \thispagestyle{empty}
  \begin{center}
    \vspace*{1cm}
    {\Huge \center \textcolor{cyan}{\bf{SNPitty}} \\ BAF Viewer \\ \small{Generated report} \\}
    
    \vspace*{2cm}
    {\Large Date:\\
    [1mm] \textcolor{cyan}{\today }\\ }
    \vfill
    
    % Name of facility
    {\it Cancer Computational Biology Center\\  Erasmus Medical Center\\  The Netherlands\\  [1mm] \today } \\
    
    % ErasmusMC Logo
    \vspace*{1cm}
    
  \end{center}
  \newpage
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                           Variant information                         %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  \section{Filtering information}
    The variants have been filtered using the following criteria: >100 DP, strand bias must be between 0.05 - 0.95, hotspot variants only.\\

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                             Region information                        %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \begin{landscape}
    \begin{figure}
    <<Chr1.19, echo=F,message=F, warning=F, out.width='9in', fig.width=15, fig.height=9>>=
      regions <-  c("Chr1-1p", "Chr1-1q", "chr19-19p", "chr19-19q")
      multiplot(createLOHPlotRegions(inputData, regions), createReadBarplotRegions(inputData, regions))
    @
    \caption{Regional overview of Chr1 and Chr19 per sample.}
    \end{figure}
  \end{landscape}
  
  \begin{landscape}
    \begin{figure}
    <<Chr9, echo=F,message=F, warning=F, out.width='9in', fig.width=15, fig.height=9>>=
      regions <-  c("chr9-9p-1", "chr9-9p-MTAP", "chr9-9p-CDKN2A", "chr9-9p-CDKN2B-AS1" , "chr9-9p-2")
      multiplot(createLOHPlotRegions(inputData, regions), createReadBarplotRegions(inputData, regions))
    @
    \caption{Regional overview of Chr9 per sample.}
    \end{figure}
  \end{landscape}

  \begin{landscape}
    \begin{figure}
    <<Chr10, echo=F,message=F, warning=F, out.width='9in', fig.width=15, fig.height=9>>=
      regions <-  c("chr10-10p", "chr10-10q-1", "chr10-10q-2",  "chr10-10q-PTEN", "chr10-10q-3", "chr10-10q-4")
      multiplot(createLOHPlotRegions(inputData, regions), createReadBarplotRegions(inputData, regions))
    @
    \caption{Regional overview of Chr10 per sample.}
    \end{figure}
  \end{landscape}

\end{document}